#pragma once
#include <grp.h>
#include <pwd.h>
#include <stdbool.h>

/**
 * @struct user_data
 * Contains user data.
 */
struct user_data {
  /**
   * @brief user group.
   */
  struct group* grp;

  /**
   * @brief user password.
   */
  struct passwd* pwd;

  /**
   * @brief user password hash.
   */
  const char* hash;

  /**
   * @brief true, if user has no password registered.
   */
  bool no_password;
};

/**
 * @fn init_user_data
 * @param uid: user id
 * @return instance of user_data
 */
struct user_data* init_user_data(int uid);

/**
 * @fn free_user_data
 * @param ud: user data instance
 * Removes user_data instance.
 */
void free_user_data(struct user_data* ud);

/**
 * @fn drop_privileges
 * @param ud: user data instance
 * Drops user privileges.
 */
void drop_privileges(struct user_data* ud);
