add_custom_target(code_documentation 
	COMMAND doxygen ${CMAKE_SOURCE_DIR}/Doxygen.config
	WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
)
