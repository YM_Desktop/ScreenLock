#pragma once

enum INPUT_VIEW_MODE { NATIVE, SECURED };

/**
 * @struct password_input_handler
 * Contains info related to user password
 */
struct password_input_handler {
  /**
   * @brief user input data.
   */
  char* input;

  /**
   * @brief number of inserted_chars.
   */
  unsigned short inserted_chars;

  /**
   * @brief user password hash.
   */
  const char* approved_hash;

  /**
   * @brief maximum length of user input.
   */
  const unsigned short max_input_len;

  /**
   * @brief input view mode
   * (Makes/Unmakes inputted chars visible)
   */
  enum INPUT_VIEW_MODE view_mode;
};

/**
 * @fn free_password_input
 * @param pih: password_input_handler instance
 * Clears data from password_input_handler instance.
 */
void free_password_input(struct password_input_handler* pih);

/**
 * @fn reset_password_input
 * @param pih: password_input_handler instance
 * Resets data in password_input_handler instance.
 */
void reset_password_input(struct password_input_handler* pih);

/**
 * @fn update_password_input
 * @param pih: password_input_handler instance
 * Updates input field in password_input_handler instance.
 */
void update_password_input(struct password_input_handler* pih, char input_char);

/**
 * @fn password_input_match
 * @param pih: password_input_handler instance
 * Checks whether input from user is the actual password.
 * @return 1 if true, 0 otherwise.
 */
int password_input_match(struct password_input_handler* pih);

/**
 * @fn init_password_input_handler
 * @param hash: password hash.
 * Creates and returns instance of password_input_handler.
 * @return pointer to instance.
 */
struct password_input_handler* init_password_input_handler(const char* hash);
