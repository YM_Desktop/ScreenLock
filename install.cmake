set(PROJECT_NAME ScreenLock)
set(PROJECT_DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
execute_process(
	COMMAND bash -c "chown root ${PROJECT_NAME}"
	WORKING_DIRECTORY ${PROJECT_DESTINATION}
)
execute_process(
	COMMAND bash -c "chmod u+s ${PROJECT_NAME}"
	WORKING_DIRECTORY ${PROJECT_DESTINATION}
)
