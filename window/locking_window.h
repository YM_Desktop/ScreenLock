#pragma once
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xrandr.h>
#include <X11/keysym.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <user_data.h>

/**
 * @struct lock
 * Contains window related handlers
 */
struct lock {
  /**
   * @brief screen id.
   */
  int screen;

  /**
   * @brief root window handler.
   */
  Window root;

  /**
   * @brief current window handler.
   */
  Window win;

  /**
   * @brief pixmap handler.
   */
  Pixmap pmap;
};

/**
 * @struct xrandr
 * Contains xrandr related data.
 */
struct xrandr {
  /**
   * @brief active screen id.
   */
  int active;

  /**
   * @brief ...
   */
  int evbase;

  /**
   * @brief ...
   */
  int errbase;
};

/**
 * @struct locking_window
 * Contains main handlers of X lib
 */
struct locking_window {

  /**
   * @brief xrandr handler
   */
  struct xrandr rr;

  /**
   * @brief locks instance
   */
  struct lock** locks;

  /**
   * @brief display handler.
   */
  Display* dpy;

  /**
   * @brief number of screens.
   */
  int nscreens;

  /**
   * @brief font information
   */
  XFontStruct* font_info;
};

/**
 * @fn init_xlib
 * Inits Xlib handlers and sets up connection to X server.
 * @param lw: locking window instance
 * @return possible error message.
 */
const char* init_xlib(struct locking_window* lw);

/**
 * @fn free_xlib
 * Closes window and connection to X server.
 * @param lw: locking window instance
 */
void free_xlib(struct locking_window* lw);

/**
 * @fn lock_all_screens
 * Closes window and connection to X server.
 * @param lw: locking window instance
 * @return possible error message.
 */
const char* lock_all_screens(struct locking_window* lw);

/**
 * @fn run_ui
 * Closes window and connection to X server.
 * @param ud: user data instance.
 * @return possible error message.
 */
const char* run_ui(struct user_data* ud);
