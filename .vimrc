command! Init  :!cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=build
command! Build :!cmake --build build --parallel && cmake --install build
command! Test  :!LD_LIBRARY_PATH=${PWD}/build/lib ctest --test-dir build/test
command! TestV :!LD_LIBRARY_PATH=${PWD}/build/lib ctest --test-dir build/test --verbose
